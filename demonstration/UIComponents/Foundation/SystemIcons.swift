//
//  SystemIcons.swift
//  demonstration
//
//  Created by Fernando on 02/01/22.
//

import Foundation

enum SystemIcons: String {
    case list = "list.bullet.rectangle.portrait"
    case dollarSign = "dollarsign.circle"
}
